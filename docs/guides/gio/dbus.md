---
title: D-Bus
---

# D-Bus

This is a fairly long guide on how to use D-Bus in GJS. It covers most topics,
from low-level usage to high-level conveniences and how to easily use D-Bus in
GTK applications. If you're already familiar with D-Bus or you just want to get
started the quickest way possible, you can jump to the high-level code examples
for [Clients](#high-level-proxies) and [Services](#exporting-interfaces). 

While working with D-Bus you can use [D-Spy][dspy] or the
built-in inspector in [GNOME Builder][gnome-builder] to introspect D-Bus on your
desktop. [The guide for `GLib.Variant` values](/guides/glib/gvariant.html) will
also be useful while learning how to use D-Bus.

[dbus]: https://dbus.freedesktop.org/
[gvariant]: https://gjs-docs.gnome.org/glib20/glib.variant
[dspy]: https://flathub.org/apps/details/org.gnome.dspy
[gnome-builder]: https://flathub.org/apps/details/org.gnome.Builder


**Related Guides**

* [Asynchronous Programming Guide](/guides/gjs/asynchronous-programming.html)
* [GVariant Guide](/guides/glib/gvariant.html)


## Introduction to D-Bus

[D-Bus][dbus] is a messaging system that can be used to communicate between
processes, enforce single-instance applications, start those services on demand
and more. This section is an overview of D-Bus including the structure of
services, bus types and connections.


### Bus Structure

To get our bearings, let's first take a look at the hierarchy of a common D-Bus
service that most users will have on their desktop. [UPower][upower] is a good
example because it exports an object for the application itself and additional
objects for each power device (eg. laptop battery):

```
org.freedesktop.UPower
    /org/freedesktop/UPower
        org.freedesktop.DBus.Introspectable
        org.freedesktop.DBus.Peer
        org.freedesktop.DBus.Properties
        org.freedesktop.UPower
    /org/freedesktop/UPower/devices/battery_BAT0
        org.freedesktop.DBus.Introspectable
        org.freedesktop.DBus.Peer
        org.freedesktop.DBus.Properties
        org.freedesktop.UPower.Device
```

[upower]: https://upower.freedesktop.org/


#### Names

At the top-level we have the well-known name, `org.freedesktop.UPower`. This is
a "reverse-DNS" style name that should also be the [Application ID][app-id]. The
D-Bus server will resolve this to a unique name like `:1.67`, sometimes referred
to as the name owner. Think of this like a DNS server resolving a web site
address (`www.gnome.org`) to an IP address (`8.43.85.13`).

This facilitates what is known as [D-Bus Activation][dbus-activation] which
allows the D-Bus server to automatically start services when they are accessed
at a well-known name. Once the process starts it will "own the name", and thus
become the name owner.

[app-id]: https://developer.gnome.org/documentation/tutorials/application-id.html
[dbus-activation]: https://developer.gnome.org/documentation/guidelines/maintainer/integrating.html#d-bus-activation


#### Object Paths

At the second level we have two object paths: `/org/freedesktop/UPower`
representing the application and `/org/freedesktop/UPower/devices/battery_BAT0`
representing a laptop battery. These objects aren't really used for anything
themselves, but rather are containers for various interfaces.

Notice the convention of using the well-known name (in the form of a path)
as the base path for objects belonging to the service.


#### Interfaces

At the third level we have interfaces and that's what we're really interested
in. Just like a GObject these can have methods, properties and signals.

Like object paths, the convention is to use the well-known name as the base for the interface name. Each path will also have a set of common
interfaces, those beginning with `org.freedesktop.DBus`, for introspecting
the service and property management.

In D-Bus the method arguments and return values, property values and signal
parameters are all D-Bus Variants. When using the GNOME API these are
[`GLib.Variant`][gvariant] objects, which is covered in the
[GVariant guide](/guides/glib/gvariant.html). Note in particular that D-Bus does
not support maybe types (`m`), which means there is no `null` value.


### Bus Connections

A [`Gio.DBusConnection`][gdbusconnection] is used to communicate with the
services on a bus and an average desktop has two bus types. 

The system bus is used for services that are independent of a user session and
often represent real devices like a laptop battery (UPower) or Bluetooth devices
(bluez). You probably won't be exporting any services on this bus, but you might
be a client to a service.

The session bus is far more common to use and many user applications and desktop
services are exported here. Some examples include notification servers, search
providers for GNOME Shell, or even regular applications such as the file
browser that wants to expose actions like `EmptyTrash()`.

You can get a bus connection using the convenience properties in GJS:

```js
const sessionConnection = Gio.DBus.session;
const systemConnection = Gio.DBus.system;
```

[gdbusconnection]: https://gjs-docs.gnome.org/gio20/gio.dbusconnection


### Interface Definitions

Most projects declare interface definitions in XML, either as files or inline in
the code. In GJS, describing exported interfaces in XML is mandatory. GJS
includes convenience functions for creating client proxies directly from an XML
string, which is covered in the [High-Level Proxies](#high-level-proxies)
section.

The official D-Bus documentation has [API Design Guidelines][dbus-api-design],
so we'll just show a simple example of an XML definition:

```xml
<node>
  <!-- Notice that neither the well-known name or object path are defined -->
  <interface name="guide.gjs.Test">
  
    <!-- A method with no arguments and no return value -->
    <method name="SimpleMethod"/>
    
    <!-- A method with both arguments and a return value -->
    <method name="ComplexMethod">
      <arg type="s" direction="in" name="input"/>
      <arg type="u" direction="out" name="length"/>
    </method>
    
    <!-- A read-only property -->
    <property name="ReadOnlyProperty" type="s" access="read"/>
    
    <!-- A read-write property -->
    <property name="ReadWriteProperty" type="b" access="readwrite"/>
    
    <!-- A signal with two arguments -->
    <signal name="TestSignal">
      <arg name="type" type="s"/>
      <arg name="value" type="b"/>
    </signal>
  </interface>
</node>
```

[dbus-api-design]: https://dbus.freedesktop.org/doc/dbus-api-design.html


## Clients

Clients for D-Bus services are often referred to as proxies, and libraries for
many services like Evolution Data Server are either wrappers or subclasses of
[`Gio.DBusProxy`][gdbusproxy]. To get started using proxies quickly, jump ahead
to [High-Level Proxies](#high-level-proxies) and use the convenience classes
offered by GJS.

It was mentioned earlier that some services support D-Bus Activation which
allows the D-Bus server to start the service process automatically. See the
documentation for [`Gio.BusNameWatcherFlags`][gbusnamewatcherflags],
[`Gio.DBusCallFlags`][gdbuscallflags] and [`Gio.DBusProxyFlags`][gdbusproxyflags]
for more information about this and other flags you can pass on the client side.

[gdbusproxy]: https://gjs-docs.gnome.org/gio20/gio.dbusproxy
[gbusnamewatcherflags]: https://gjs-docs.gnome.org/gio20/gio.busnamewatcherflags
[gdbuscallflags]: https://gjs-docs.gnome.org/gio20/gio.dbuscallflags
[gdbusproxyflags]: https://gjs-docs.gnome.org/gio20/gio.dbusproxyflags


### Watching a Name

To know when a D-Bus service appears and vanishes from the message bus, you can
watch for the well-known name to become owned by a name owner. Note that you can
still hold a client proxy and be connected to signals while a service is
unavailable though.

<<< @/../src/guides/gio/dbus/gdbusNameWatcher.js{js}

### Direct Calls

In this section, we'll see by example that all operations we perform as a client
are actually performed on a bus connection. Whether it's calling methods,
getting and setting property values or connecting to signals, these are all
ultimately being passed through a bus connection.

Although you usually won't need to do this, it is sometimes more convenient if
you only need to perform a single operation. In other cases it may be useful to
work around problems with introspected APIs that use D-Bus since the data
exchanged as `GLib.Variant` objects are fully supported.

#### Method Calls

Below is an example of sending a libnotify notification and getting the
resulting reply ID. In this first example we will demonstrate the following
steps:

1. Packing the method arguments
2. Calling the method
3. Unpacking the method reply
4. Handling D-Bus errors

<<< @/../src/guides/gio/dbus/gdbusConnectionMethods.js{js}

#### Properties

Getting or setting the value of properties are also just method calls,
made to the standard `org.freedesktop.DBus.Properties` interface. The name of
the interface holding the property is passed as the first argument of the method
arguments.

<<< @/../src/guides/gio/dbus/gdbusConnectionProperties.js{js}


#### Signal Connections

Connecting signal handlers directly on a connection is also possible. See the
[`Gio.DBusConnection.signal_subscribe()`][conn-signal-subscribe] documentation
for details about signal matching.

<<< @/../src/guides/gio/dbus/gdbusConnectionSignals.js{js}

[conn-signal-subscribe]: https://gjs-docs.gnome.org/gio20/gio.dbusconnection#method-signal_subscribe


### Low-Level Proxies

The reason [`Gio.DBusProxy`][gdbusproxy] objects are so much more convenient is
they allow you to treat the collection of methods, properties and signals of a
service interface as a discrete object. They can automatically cache the values
of properties as they change, connect and group signals, watch for the name
owner appearing or vanishing, and generally reduce the amount of code you have
to write.

Note that the synchronous constructors will block the main thread while
getting the D-Bus connection and caching the initial property values. To avoid
this, you can use asynchronous constructors like `Gio.DBusProxy.new()` and
`Gio.DBusProxy.new_for_bus()`.

<<< @/../src/guides/gio/dbus/gdbusProxy.js{js}

[g-interface-info]: https://gjs-docs.gnome.org/gio20/gio.dbusproxy#property-g_interface_info


### High-Level Proxies

The D-Bus conveniences in GJS are the easiest way to get a client and cover most
use cases. All you need to do is call `Gio.DBusProxy.makeProxyWrapper()` with
the interface XML and it will create a reusable class you can use to create
proxies.


#### Creating a Proxy Wrapper

Below is an example of how a proxy wrapper is created from an XML interface:

<<< @/../src/guides/gio/dbus/gdbusProxyWrapper.js#create{js}

Now that we have created a reusable class, we can easily create client proxies
for an interface. The proxy will be created synchronously or asynchronously
depending on whether a callback is passed in the constructor arguments.

The arguments for the constructor are as follows:

* `bus` - a `Gio.DBusConnection`
* `name` - a well-known name
* `object` - an object path
* `asyncCallback` - an optional callback
* `cancellable` - an optional `Gio.Cancellable`
* `flags` - an optional `Gio.DBusProxyFlags` value

The arguments passed to the `asyncCallback` are as follows:

* `proxy` - a `Gio.DBusProxy`, or `null` on failure
* `error` - a `GLib.Error`, or `null` on success


Here are examples for both synchronous and asynchronous construction:

<<< @/../src/guides/gio/dbus/gdbusProxyWrapper.js#construct{js}

#### Using a Proxy Wrapper

With our proxy wrapper class, we can create a `Gio.DBusProxy` that has methods,
properties and signals all set up for us.

Methods are defined for three variants: a synchronous variant, asynchronous
with callback, and a variant that returns a `Promise`:

<<< @/../src/guides/gio/dbus/gdbusProxyWrapper.js#methods{js}

Properties work like native JavaScript properties and you can watch the
[`g-properties-changed`] signal to be notified of changes:

<<< @/../src/guides/gio/dbus/gdbusProxyWrapper.js#properties{js}

Signals are connected and disconnected with the functions `connectSignal()`
and `disconnectSignal()`, so they don't conflict with the GObject methods:

<<< @/../src/guides/gio/dbus/gdbusProxyWrapper.js#signals{js}

[`g-properties-changed`]: https://gjs-docs.gnome.org/gio20/gio.dbusproxy#signal-g-properties-changed

## Services

There are a number of reasons why exporting services over D-Bus can be useful
for an application developer. It can help you establish a client-server
architecture to separate the backend from the front-end, but it can also provide
a language agnostic entry point for your application.


### Owning a Name

The first thing we're going to cover is how to acquire a well-known name on a
bus connection and at what point you will want to actually export your service.
This is similar to watching a name:

<<< @/../src/guides/gio/dbus/gdbusNameOwner.js{js}


### Exporting Interfaces

Now that we know how to own a well-known name, let's get to exporting our
service interface. We'll use the same XML definition from the earlier example,
since it has one of everything:

<<< @/../src/guides/gio/dbus/gdbusService.js#interface{js}


GJS provides a convenience function for creating a service, which takes an XML
definition and a class instance. The function is called
[`Gio.DBusExportedObject.wrapJSObject()`][gjs-wrapjsobject] and is documented
with the other Gio overrides in GJS.

The class can be either a plain JavaScript class or a GObject subclass, as long
as it implements the defined methods and properties. Changes to property values
and other signals must be emitted manually.

<<< @/../src/guides/gio/dbus/gdbusService.js#service{js}

When a property is set by a client of your service, the setter will be passed
the return value of [`GLib.Variant.deepUnpack()`][gjs-deepunpack]. When a client
requests the value of a property, the getter may return either a native value
(e.g. `'a string'`) or a `GLib.Variant` that matches the expected signature.

Note that the object returned by `Gio.DBusExportedObject.wrapJSObject()` and the
class instance are separate. The class definition above expects that the D-Bus
object has been assigned to `this._impl`, so that it can emit property changes
and signals.

If you choose to follow the same pattern, just be sure that is done before you
export the service:

<<< @/../src/guides/gio/dbus/gdbusService.js#usage{js}

[gjs-deepunpack]: https://gjs.guide/guides/glib/gvariant.html#deepunpack
[gjs-wrapjsobject]: https://gjs-docs.gnome.org/gjs/overrides.md#gio-dbusexportedobject-wrapjsobject


## Other APIs

There are a number of APIs in the GNOME platform that can make use of D-Bus,
notably `Gio.Application`, `Gio.Action` and `Gio.Menu`. Although a little less
flexible, these higher-level APIs make exporting functionality on D-Bus
extremely easy and the built-in support in GTK makes them an excellent choice
for applications.

In this section we'll cover the basics of `Gio.Action` and `Gio.Menu`, which
provide an easy way to export functionality and structured presentation. The
GNOME Developer Documentation for [`GActions`][gaction-tutorial] and
[`GMenus`][gmenu-tutorial] have a more complete overview of these APIs.

[gaction-tutorial]: https://developer.gnome.org/documentation/tutorials/actions.html
[gmenu-tutorial]: https://developer.gnome.org/documentation/tutorials/menus.html


### GAction

[`Gio.Action`][gaction] is actually a GObject Interface that can be implemented
by objects, but you will almost always use the provided implementation
[`Gio.SimpleAction`][gsimpleaction]. There are basically two kinds of actions: a
functional type that emits a signal when activated and stateful actions that
hold some kind of value.

`Gio.Action` objects are usually added to objects that implement the 
[`Gio.ActionGroup`][gactiongroup] and [`Gio.ActionMap`][gactionmap] interfaces.
The [`Gio.SimpleActionGroup`][gsimpleactiongroup] implementation
supports both of these.

Below are a few examples of how to create each type of action:

<<< @/../src/guides/gio/dbus/gdbusGAction.js#action{js}

Once you've created actions, you will want to add them to a group so that they
can be exported over D-Bus. The exported actions will be playing the part of the
"server" in this case.

<<< @/../src/guides/gio/dbus/gdbusGAction.js#action-group{js}

Now any number of other processes can act as clients by using a
[`Gio.DBusActionGroup`][gdbusactiongroup]. This class implements the
`Gio.ActionGroup` interface, but not the `Gio.ActionMap` interface. This means
clients can activate actions and change their state value, but they can not
enable, disable, add or remove them.

<<< @/../src/guides/gio/dbus/gdbusGAction.js{js}

What makes `Gio.Action` even more convenient, is that GTK already knows how to
do all of this for you. Simply insert the `Gio.ActionGroup` into any
`Gtk.Widget` and that widget or any of its children that implement the
[`Gtk.Actionable`][gtkactionable] interface can activate or set the state of the
action.

<<< @/../src/guides/gio/dbus/gdbusGActionWindow.js{js}

[gaction]: https://gjs-docs.gnome.org/gio20/gio.action
[gactiongroup]: https://gjs-docs.gnome.org/gio20/gio.actiongroup
[gactionmap]: https://gjs-docs.gnome.org/gio20/gio.actionmap
[gsimpleaction]: https://gjs-docs.gnome.org/gio20/gio.simpleaction
[gsimpleactiongroup]: https://gjs-docs.gnome.org/gio20/gio.simpleactiongroup
[gdbusactiongroup]: https://gjs-docs.gnome.org/gio20/gio.dbusactiongroup
[gtkactionable]: https://gjs-docs.gnome.org/gtk40/gtk.actionable


### GMenu

[`Gio.MenuModel`][gmenumodel] is another GObject Interface for defining ordered,
nested groups of menu items, sections and submenus. The defacto implementation
of this interface is [`Gio.Menu`][gmenu].

Unlike `Gio.Action`, menu models contain presentation information like labels
and icon names. It's also possible to define menu models in XML UI files, but
we're only going to cover the basic API usage in GJS here because we're really
just covering this for D-Bus usage.

<<< @/../src/guides/gio/dbus/gdbusGMenuExport.js{js}

Now, assuming we have a remote process exporting both the action group and menu
model from above, we can get clients for both and populate a menu:

<<< @/../src/guides/gio/dbus/gdbusGMenuWindow.js{js}

[gmenu]: https://gjs-docs.gnome.org/gio20/gio.menu
[gmenumodel]: https://gjs-docs.gnome.org/gio20/gio.menumodel
[gdbusmenumodel]: https://gjs-docs.gnome.org/gio20/gio.dbusmenumodel

