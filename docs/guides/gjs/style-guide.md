---
title: Style Guide
---
# Style Guide

This guide documents how to use the official GJS ESlint configuration, as well
as other preferred styles that can't be expressed by a linter configuration.

It also includes a basic introduction to setting up a project to use
`.eslintrc.yml` and `.editorconfig` files, to help reduce manual work
for developers.

## ESLint

::: tip
GNOME Shell includes one additional global variable called [`global`].
:::

[ESLint] is a well known linter and static analysis tool for JavaScript, used by
both GJS and GNOME Shell. It's used by many projects to maintain code quality,
enforce coding standards, catch potential errors, and improve code consistency.

The [recommended configuration] includes rules for static analysis, deprecated
syntax and a list of all the global variables for the environment. Put the
`.eslintrc.yml` in the root of your project directory, and your IDE will
provide real-time diagnostics and warnings.

::: details <code>.eslintrc.yml</code>
<<< @/../src/guides/gjs/style-guide/.eslintrc.yml{yml}
:::

ESLint is transitioning to a new flat configuration that uses ES Modules. To
use this configuration, be sure your project has a `package.json` file with
`"sourceType": "module"`.

::: details <code>eslint.config.js</code>
<<< @/../src/guides/gjs/style-guide/eslint.config.js{js}
:::

To use the [ESLint CLI] to lint your project:

```sh
npx eslint .
```

[ESLint]: https://eslint.org
[ESLint CLI]: https://eslint.org/docs/latest/use/command-line-interface
[`global`]: https://gjs-docs.gnome.org/shell13/shell.global
[recommended configuration]: https://gitlab.gnome.org/World/javascript/gjs-guide/blob/main/src/guides/gjs/style-guide/eslint.config.js

### Continuous Integration

In most projects, it is recommended practice to run tests on every pull request
before merging into the main branch. Below are two example CI configurations
for running ESLint with GitLab and GitHub.

::: details GitLab (<code>.gitlab-ci.yml</code>)
<<< @/../src/guides/gjs/style-guide/.gitlab-ci.yml{yml}
:::

::: details GitHub (<code>.github/workflows/eslint.yml</code>)
<<< @/../src/guides/gjs/style-guide/eslint.yml{yml}
:::

## Prettier

[Prettier] is another popular tool for JavaScript projects, renowned for its
lack of options. It focuses specifically on formatting code, and won't catch
logic errors or anti-patterns like ESLint.

Below is a sample configuration (with almost all available options), set to
resemble the code style used by many GJS applications:

<<< @/../src/guides/gjs/style-guide/.prettierrc.yml{yml}

[Prettier]: https://prettier.io

## EditorConfig

[EditorConfig] is a more general formatting tool, targeted directly at IDEs
like GNOME Builder and VSCode. It's used to tell an editor to trim trailing
whitespace, what indentation to use, and other similar preferences.

Below is the `.editorconfig` file used in the GJS project:

<<< @/../src/guides/gjs/style-guide/.editorconfig#snippet{ini}


[EditorConfig]: https://editorconfig.org/

## Code Conventions

The following guidelines are general recommendations and coding conventions
followed by many GJS projects. As general rule, you should take advantage of
modern language features, both in JavaScript and GJS.

### Files and Imports

::: tip
GJS has supported ESModules since GNOME 40, and GNOME Shell extensions are
required to use them since GNOME 45.
:::

JavaScript file names should be `lowerCamelCase` with a `.js` extension, while
directories should be short and `lowercase`:

```sh:no-line-numbers
js/misc/extensionSystem.js
js/ui/panel.js
```

Use `PascalCase` when importing modules and classes

```js:no-line-numbers
import * as Util from 'resource:///gjs/guide/Example/js/util.js';
```

Keep library, module and local imports separated by a single line.

```js:no-line-numbers
import Gio from 'gi://Gio';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import * as Util from './lib/util.js';
```

### GObject

::: tip
See [GObject Basics](../gobject/basics.md) for more details about using GObject
in JavaScript.
:::

#### Properties

When possible, set all properties when constructing an object, which is cleaner
and avoids extra property notifications.

<<< @/../src/guides/gjs/style-guide/gobject.js#properties-construct


Using `camelCase` property accessors is preferred by many GNOME projects. GJS
can automatically convert GObject property names, except when used as a string.

<<< @/../src/guides/gjs/style-guide/gobject.js#properties-camelcase


#### Asynchronous Operations

Use [`Gio._promisify()`](asynchronous-programming.md#promisify-helper) to enable
`async`/`await` with asynchronous methods in platform libraries:

```js:no-line-numbers
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

Gio._promisify(Gio.File.prototype, 'delete_async');

const file = Gio.File.new_for_path('file.txt');
await file.delete_async(GLib.PRIORITY_DEFAULT, null /* cancellable */);
```

### JavaScript

#### Variables and Exports

Use  `const` when the value will be bound to a static value, and `let` when you
need a mutable variable:

<<< @/../src/guides/gjs/style-guide/javascript.js#variable-mutability


The `var` statement should be avoided, since it has unexpected behavior like
[hoisting]. Although it was used in older code to make members of a script
public, `export` should now be used in all new code:

<<< @/../src/guides/gjs/style-guide/javascript.js#variable-exports


[hoisting]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/var#hoisting

#### Classes and Functions

Define classes with [`class`] and override the standard `constructor()` when
subclassing GObject classes:

<<< @/../src/guides/gjs/style-guide/javascript.js#class-definition


Use [arrow functions] for inline callbacks and [`Function.prototype.bind()`]
for larger functions.

<<< @/../src/guides/gjs/style-guide/javascript.js#arrow-functions


[`class`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Classes
[arrow functions]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Functions/Arrow_functions
[`Function.prototype.bind()`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
