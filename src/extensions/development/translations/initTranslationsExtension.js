import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';


export default class ExampleExtension extends Extension {
    constructor(metadata) {
        super(metadata);

        this.initTranslations('example@gjs.guide');
    }

    enable() {
        console.debug(_('Enabling %s').format(this.metadata.name));
    }

    disable() {
        console.debug(_('Disabling %s').format(this.metadata.name));
    }
}
