import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';


/**
 * Create a new indicator.
 *
 * @param {string} [name] - A name for the indicator
 * @returns {PanelMenu.Button} A new indicator
 */
export function createIndicator(name = 'Unknown') {
    return new PanelMenu.Button(0.0, name, true);
}
