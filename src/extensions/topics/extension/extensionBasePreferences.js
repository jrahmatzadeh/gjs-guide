import {ExtensionPreferences} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';


let extensionObject, extensionSettings;

// Getting the extension object by UUID
extensionObject = ExtensionPreferences.lookupByUUID('example@gjs.guide');
extensionSettings = extensionObject.getSettings();
console.log(extensionObject.metadata);

// Getting the extension object by URL
extensionObject = ExtensionPreferences.lookupByURL(import.meta.url);
extensionSettings = extensionObject.getSettings();
console.log(extensionObject.metadata);
