/* eslint-disable no-invalid-this */

import {Extension, InjectionManager} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import {Panel} from 'resource:///org/gnome/shell/ui/panel.js';

// #region example
export default class ExampleExtension extends Extension {
    enable() {
        this._injectionManager = new InjectionManager();

        // Overriding a method with an *arrow function*
        this._injectionManager.overrideMethod(Panel.prototype, 'toggleCalendar',
            originalMethod => {
                return args => {
                    console.debug(`${this.metadata.name}: toggling calendar`);
                    originalMethod.call(Main.panel, ...args);
                };
            });

        // Overriding a method with a *function expression*
        this._injectionManager.overrideMethod(Panel.prototype, 'toggleQuickSettings',
            originalMethod => {
                const metadata = this.metadata;

                return function (...args) {
                    console.debug(`${metadata.name}: toggling quick settings`);
                    originalMethod.call(this, ...args);
                };
            });
    }

    disable() {
        this._injectionManager.clear();
        this._injectionManager = null;
    }
}
// #endregion example

