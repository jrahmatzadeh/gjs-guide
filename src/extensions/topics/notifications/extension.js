import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Gio from 'gi://Gio';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
// #region imports
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as MessageTray from 'resource:///org/gnome/shell/ui/messageTray.js';
// #endregion imports


function _mainNotify() {
    // #region main-notify
    Main.notify('Simple Notification', 'A notification with a title and body');
    // #endregion main-notify
}

function _mainNotifyError() {
    // #region main-notify-error
    try {
        throw Error('File not found');
    } catch (e) {
        Main.notifyError('Failed', e.message);
    }
    // #endregion main-notify-error
}

function _sourceSystem() {
    // #region source-system
    const systemSource = MessageTray.getSystemSource();

    const systemNotification = new MessageTray.Notification({
        source: systemSource,
        title: 'System Notification',
        body: 'This notification will appear to come from the system',
    });
    systemSource.addNotification(systemNotification);
    // #endregion source-system
}

// #region policy
const NotificationPolicy = GObject.registerClass(
class NotificationPolicy extends MessageTray.NotificationPolicy {
    /**
     * Whether notifications will be shown.
     *
     * @type {boolean}
     */
    get enable() {
        return true;
    }

    /**
     * Whether sound will be played.
     *
     * @type {boolean}
     */
    get enableSound() {
        return true;
    }

    /**
     * Whether the notification will popup outside of the tray.
     *
     * @type {boolean}
     */
    get showBanners() {
        return true;
    }

    /**
     * Whether the notification will always be expanded.
     *
     * @type {boolean}
     */
    get forceExpanded() {
        return false;
    }

    /**
     * Whether the notification will be shown on the lock screen.
     *
     * @type {boolean}
     */
    get showInLockScreen() {
        return false;
    }

    /**
     * Whether the notification content will be shown on the lock screen.
     *
     * @type {boolean}
     */
    get detailsInLockScreen() {
        return false;
    }

    /**
     * Called when the source is added to the message tray
     */
    store() {
    }
});
// #endregion policy

// #region source-custom
let notificationSource = null;

function getNotificationSource() {
    if (!notificationSource) {
        const notificationPolicy = new NotificationPolicy();

        notificationSource = new MessageTray.Source({
            // The source name (e.g. application name)
            title: _('Notifications Example'),
            // An icon for the source, used a fallback by notifications
            icon: new Gio.ThemedIcon({name: 'dialog-information'}),
            // Same as `icon`, but takes a themed icon name
            iconName: 'dialog-information',
            // The notification policy
            policy: notificationPolicy,
        });

        // Reset the notification source if it's destroyed
        notificationSource.connect('destroy', _source => {
            notificationSource = null;
        });
        Main.messageTray.add(notificationSource);
    }

    return notificationSource;
}
// #endregion source-custom

function showNotification() {
    const customSource = getNotificationSource();

    // #region notification-new
    const notification = new MessageTray.Notification({
        // The source of the notification
        source: customSource,
        // A title for the notification
        title: _('Custom Notification'),
        // The content of the notification
        body: _('This notification uses a custom source and policy'),
        // An icon for the notification (defaults to the source's icon)
        gicon: new Gio.ThemedIcon({name: 'dialog-warning'}),
        // Same as `gicon`, but takes a themed icon name
        iconName: 'dialog-warning',
        // The urgency of the notification
        urgency: MessageTray.Urgency.NORMAL,
    });
    // #endregion notification-new

    // #region notification-destroy
    notification.connect('destroy', (_notification, reason) => {
        if (reason === MessageTray.NotificationDestroyedReason.DISMISSED)
            console.debug('The user closed the notification');
    });
    // #endregion notification-destroy

    // #region action-default
    notification.connect('activated', _notification => {
        console.debug(`${notification.title}: notification activated`);
    });
    // #endregion action-default

    // #region action-clear
    notification.clearActions();
    // #endregion action-clear

    // #region action-add
    notification.addAction(_('Close'), () => {
        console.debug('"Close" button activated');
    });
    // #endregion action-add

    notificationSource.addNotification(notification);

    return GLib.SOURCE_CONTINUE;
}

export default class NotificationsExtension extends Extension {
    constructor(metadata) {
        super(metadata);

        this._timeoutId = null;
    }

    enable() {
        // Show a notification once per minute
        showNotification();
        this._timeoutId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 60,
            showNotification);
    }

    disable() {
        if (this._timeoutId) {
            GLib.Source.remove(this._timeoutId);
            this._timeoutId = null;
        }
    }
}
