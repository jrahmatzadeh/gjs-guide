import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';

export default class MyTestExtension extends Extension {
    enable() {
        this._settings = this.getSettings();
        console.log(_('This is a translatable text'));
    }

    disable() {
        this._settings = null;
    }
}
