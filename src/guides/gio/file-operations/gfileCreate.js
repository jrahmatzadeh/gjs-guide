import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const file = Gio.File.new_for_path('test-file.txt');

const outputStream = await file.create_async(Gio.FileCreateFlags.NONE,
    GLib.PRIORITY_DEFAULT, null);

const bytes = new GLib.Bytes('some file content');
const bytesWritten = await outputStream.write_bytes_async(bytes,
    GLib.PRIORITY_DEFAULT, null);
