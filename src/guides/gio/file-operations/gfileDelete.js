import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const file = Gio.File.new_for_path('test-file.txt');

await file.delete_async(GLib.PRIORITY_DEFAULT, null);
