import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const file = Gio.File.new_for_path('test-file.txt');

const bytes = new GLib.Bytes('some file contents');
const [etag] = await file.replace_contents_bytes_async(bytes, null, false,
    Gio.FileCreateFlags.REPLACE_DESTINATION, null);
