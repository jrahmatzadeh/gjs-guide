import Gio from 'gi://Gio';


try {
    const proc = Gio.Subprocess.new(['sleep', '10'],
        Gio.SubprocessFlags.NONE);

    // NOTE: triggering the cancellable passed to these functions will only
    //       cancel the function NOT the process.
    const cancellable = new Gio.Cancellable();

    // Strictly speaking, the only error that can be thrown by
    // this function is Gio.IOErrorEnum.CANCELLED.
    await proc.wait_async(cancellable);

    // The process has completed and you can check the exit status or
    // ignore it if you just need notification the process completed.
    if (proc.get_successful())
        console.log('the process succeeded');
    else
        console.log('the process failed');
} catch (e) {
    logError(e);
}
