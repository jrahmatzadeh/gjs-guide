import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const loop = new GLib.MainLoop(null, false);
const file = Gio.File.new_for_path('test-file.txt');

// GTask-based operations invoke a callback when the task completes
file.delete_async(GLib.PRIORITY_DEFAULT, null, (_file, result) => {
    console.log('This callback was invoked because the task completed');

    try {
        file.delete_finish(result);
    } catch (e) {
        logError(e);
    }
});

await loop.runAsync();
