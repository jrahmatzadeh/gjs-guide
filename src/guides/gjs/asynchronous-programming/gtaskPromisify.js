import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


Gio._promisify(Gio.InputStream.prototype, 'read_bytes_async',
    'read_bytes_finish');


const inputStream = new Gio.UnixInputStream({fd: 0});

try {
    const bytes = await inputStream.read_bytes_async(4096,
        GLib.PRIORITY_DEFAULT, null);
} catch (e) {
    logError(e, 'Failed to read bytes');
}

inputStream.read_bytes_async(
    4096,
    GLib.PRIORITY_DEFAULT,
    null,
    (stream_, result) => {
        try {
            const bytes = inputStream.read_bytes_finish(result);
        } catch (e) {
            logError(e, 'Failed to read bytes');
        }
    }
);
