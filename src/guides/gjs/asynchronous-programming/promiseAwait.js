/* eslint-disable no-await-in-loop */

import GLib from 'gi://GLib';


const loop = new GLib.MainLoop(null, false);

// Returns a Promise that randomly fails or succeeds after one second
function unreliablePromise() {
    return new Promise((resolve, reject) => {
        GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
            if (Math.random() >= 0.5)
                resolve('success');
            else
                reject(Error('failure'));

            return GLib.SOURCE_REMOVE;
        });
    });
}


// An example async function, demonstrating how Promises can be resolved
// sequentially while catching errors in a try..catch block.
async function exampleAsyncFunction() {
    try {
        let count = 0;

        while (true) {
            await unreliablePromise();
            console.log(`Promises resolved: ${++count}`);
        }
    } catch (e) {
        logError(e);
        loop.quit();
    }
}

// Run the async function
exampleAsyncFunction();


await loop.runAsync();
