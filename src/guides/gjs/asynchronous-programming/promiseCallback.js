import GLib from 'gi://GLib';


const loop = new GLib.MainLoop(null, false);

// Returns a Promise that randomly fails or succeeds after one second
function unreliablePromise() {
    return new Promise((resolve, reject) => {
        GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
            if (Math.random() >= 0.5)
                resolve('success');
            else
                reject(Error('failure'));

            return GLib.SOURCE_REMOVE;
        });
    });
}


// When using a Promise in the traditional manner, you must chain to it with
// `then()` to get the result and `catch()` to trap errors.
unreliablePromise().then(result => {
    // Logs "success"
    console.log(result);
}).catch(e => {
    // Logs "Error: failure"
    logError(e);
});

// A convenient short-hand in GJS is just passing `logError` to `catch()`
unreliablePromise().catch(logError);


await loop.runAsync();
