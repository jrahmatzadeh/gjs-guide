import GLib from 'gi://GLib';
import GObject from 'gi://GObject';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';


const MyIndicator = GObject.registerClass(
class MyIndicator extends PanelMenu.Button {
    startUpdating() {
        // When `startUpdating()` returns, we will lose our reference to
        // `sourceId`, so we will be unable to remove it from the the main loop
        const sourceId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 5,
            this.update.bind(this));
    }

    update() {
        // If the object has been destroyed, this will cause a critical error.
        //
        // The use of Function.bind() is allows `this` to be traced to the
        // JavaScript object, regardless of the GObject's state.
        this.visible = !this.visible;

        // Returning `true` or `GLib.SOURCE_CONTINUE` causes the GSource to
        // persist, so the callback will run when the next timeout is reached
        return GLib.SOURCE_CONTINUE;
    }

    _onDestroy() {
        // We don't have a reference to `sourceId` so we can't remove the
        // source from the loop. We should have assigned the source ID to an
        // object property in `_init()` like `this._timeoutId`

        super._onDestroy();
    }
});


export default class ExampleExtension extends Extension {
    enable() {
        this._indicator = new MyIndicator();

        // Each time the extension is enabled, a new GSource will added to the main
        // main loop by this function
        this._indicator.startUpdating();
    }

    disable() {
        this._indicator.destroy();
        this._indicator = null;

        // Even though we have destroyed the GObject and stopped tracing it from
        // the `indicator` variable, the GSource callback will continue to be
        // invoked every 5 seconds.
    }
}
