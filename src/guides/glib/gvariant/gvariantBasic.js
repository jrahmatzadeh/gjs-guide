import GLib from 'gi://GLib';


// Simple types work pretty much like you expect
const variantBool = GLib.Variant.new_boolean(true);

if (variantBool.get_type_string() === 'b')
    print('Variant is a boolean!');

if (variantBool.get_boolean() === true)
    print('Value is true!');


// NOTE: As of GJS v1.68 all numeric types are still `Number` values, so some
// 64-bit values may not be fully supported. `BigInt` support to come.
const variantInt64 = GLib.Variant.new_int64(-42);

if (variantInt64.get_type_string() === 'x')
    print('Variant is an int64!');

if (variantInt64.get_int64() === -42)
    print('Value is -42!');


// NOTE: GLib.Variant.prototype.get_string() returns the value and the length
const variantString = GLib.Variant.new_string('a string');
const [strValue, strLength] = variantString.get_string();

if (variantString.get_type_string() === 's')
    print('Variant is a string!');

if (variantString.get_string()[0] === 'a string')
    print('Success!');


// List of strings are also straight forward
const stringList = ['one', 'two'];
const variantStrv = GLib.Variant.new_strv(stringList);

if (variantStrv.get_type_string() === 'as')
    print('Variant is an array of strings!');

if (variantStrv.get_strv().every(value => stringList.includes(value)))
    print('Success!');
