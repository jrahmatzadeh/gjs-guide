import GLib from 'gi://GLib';


// Below is an example of a libnotify notification, ready to be sent over DBus
const notification = new GLib.Variant('(susssasa{sv}i)', [
    'gjs.guide Tutorial',
    0,
    'dialog-information-symbolic',
    'Notification Title',
    'Notification Body',
    [],
    {},
    -1,
]);

// Here is another complex variant, showing how child values marked `v` have to
// be packed like other variants.
const variantTuple = new GLib.Variant('(siaua{sv})', [
    'string',                               // a string
    -1,                                     // a signed integer
    [1, 2, 3],                              // an array of unsigned integers
    {                                       // a dictionary of string => variant
        'code-name': GLib.Variant.new_string('007'),
        'licensed-to-kill': GLib.Variant.new_boolean(true),
    },
]);

// Dictionaries with shallow, uniform value types can be packed in a single step
const shallowDict = new GLib.Variant('a{ss}', {
    'key1': 'value1',
    'key2': 'value2',
});

// Dictionaries with a varying value types use `v` and must be packed
const deepDict = new GLib.Variant('a{sv}', {
    'key1': GLib.Variant.new_string('string'),
    'key2': GLib.Variant.new_boolean(true),
});
