import GObject from 'gi://GObject';

// #region gtype-jsobject
const ExampleSubclass = GObject.registerClass({
    Properties: {
        'example-property': GObject.ParamSpec.jsobject(
            'example-property',
            'Example Property',
            'A property that holds a JavaScript Object',
            GObject.ParamFlags.READWRITE
        ),
    },
    Signals: {
        'example-signal': {
            param_types: [GObject.TYPE_JSOBJECT],
        },
    },
}, class ExampleSubclass extends GObject.Object {
    get example_property() {
        if (this._example_property === undefined)
            this._example_property = {};

        return this._example_property;
    }

    set example_property(obj) {
        if (this.example_property === obj)
            return;

        this._example_property = obj;
        this.notify('example-object');
    }

    emitExampleSignal(obj) {
        this.emit('example-signal', obj);
    }
});
// #endregion gtype-jsobject
