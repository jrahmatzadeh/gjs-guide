import GObject from 'gi://GObject';
import Gdk from 'gi://Gdk?version=4.0';
import Gtk from 'gi://Gtk?version=4.0';

// #region gvalue-return-args
// Create a GObject to pass around
const objectInstance = new GObject.Object();

// A GValue can be used to pass data via Drag-n-Drop
const dragSource = new Gtk.DragSource({
    actions: Gtk.DragAction.COPY,
});

dragSource.connect('prepare', (_dragSource, _x, _y) => {
    const value = new GObject.Value();
    value.init(GObject.Object);
    value.set_object(objectInstance);

    return Gdk.ContentProvider.new_for_value(value);
});

// The Drag-n-Drop target receives the unpacked value
const dropTarget = Gtk.DropTarget.new(GObject.Object,
    Gdk.DragAction.COPY);

dropTarget.connect('drop', (_dropTarget, value, _x, _y) => {
    if (value instanceof GObject.Object)
        console.debug('The GObject.Value was unpacked to a GObject.Object');
});
// #endregion gvalue-return-args
