import GObject from 'gi://GObject';

// #region snippet
const SubclassOne = GObject.registerClass({
}, class SubclassOne extends GObject.Object {
});

const SubclassTwo = GObject.registerClass({
    GTypeName: 'CustomName',
}, class SubclassTwo extends GObject.Object {
});

// expected output: 'Gjs_SubclassOne'
console.log(SubclassOne.$gtype.name);

// expected output: 'CustomName'
console.log(SubclassTwo.$gtype.name);
// #endregion snippet

